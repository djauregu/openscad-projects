sphere_radius = 10;
cube_length = sphere_radius * 1.5;
object_separation = sphere_radius * 2 + 4;

translate([-object_separation,0,0]) {
    union() {
        cube(cube_length, center=true);
        sphere(sphere_radius);
    }
}

intersection() {
    cube(cube_length, center=true);
    sphere(sphere_radius);
    translate([cube_length / 2, 0, 0]) {
    cube(cube_length, center=true);
    }
}

translate([object_separation,0,0]) {
    difference() {
        sphere(sphere_radius);
        cube(cube_length, center=true);
    }
}


translate([0,30,0]) {
    cube([10,10,2], center=true);
}

echo(version=version());